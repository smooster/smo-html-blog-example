# smooster blog example

You can use this example to see how to build a blog feature in smooster.

## Folder structure
* /html contains the project related files (html, css, js, images, ...)

## Installation

### clone git

    $ git clone git@bitbucket.org:smooster/smo-html-blog-example.git

  or just download it via [Downloads](https://bitbucket.org/smooster/smo-html-blog-example/downloads)

### deploy to smooster

Use the [smooster CLI](https://bitbucket.org/smooster/smooster) to create your own demo in smooster.

replace SITE_ID with the site id of the site you created in smooster

    $ smooster setup SITE_ID

then deploy the example

    $ smooster deploy initial

this will create the media assets, templates and pages. But before you can check it, you need to make some further adjustments

1. login to smooster CMS -> select your new site
2. create a folder blog
3. move the blog article page to the blog folder & edit the headline and save the page (to test with more blog articles create new pages in the blog folder with template "blog article" as often as you like)
4. click on edit folder & copy the smooster ID below the input field
5. click more & templates
6. select the blog overview template (index) -> click on edit
7. activate the liquid feature "Activate template language Liquid for this subtemplate"
8. change the folder id in the template in line 34 {% for page in site.folders.[CHANGE_BLOG_FOLDER_ID_HERE].pages.without_index.order_by_date_desc limit:5 offset:0 %}
AND in line 68  <script type="text/x-jsrender" id="news_template" data-a-id="[CHANGE_BLOG_FOLDER_ID_HERE]">
9. save the template

If you want to test the load more button, you'll need at least 6 articles in the blog folder or change the blog article amount per page in the template AND in the blog_more.coffee script file

## Usage

  Use the html/index.html as your first template file, if needed you can add more html files in /html.


## Wishlist
* None so far

## Changelog

###0.1.0
* Basic folder and file structure

## Contributing

1. Fork it ( https://bitbucket.org/smooster/smo-html-blog-example/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
