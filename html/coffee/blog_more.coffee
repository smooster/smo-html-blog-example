$(document).ready ->

    #Config
    template_id = '#news_template'
    more_button_id = '#more-blog a'
    date_id = "text_h5"
    #author_id = "author"
    headline_id = "text_h2"
    picture_id = "picture-blog1"
    teaser_id = "text_a"
    articles_per_page = 5
    #Config End

    page_counter = 1

    template = Hogan.compile($(template_id).html())

    loadNextLink =  ->
      if $("article").length > 1
        $("article").each (index, value) ->
          unless $("#next_link_#{index}").length > 0
            next_link = $('<a>', {href: $(value).attr('data-more-link'), id: "next_link_#{index}", class: "next_links"}).html("... weiter")
            $('.text p', value).last().append(next_link)

    load_next_content = ->
        page_counter += 1

        new_pages = []

        Smooster.folder.id($(template_id).attr('data-a-id'))
        Smooster.folder.without_index(true)
        Smooster.folder.per_page(articles_per_page)
        Smooster.folder.page(page_counter)
        Smooster.folder.pages().done (result) ->
          if $(result).length < articles_per_page
            $(more_button_id).fadeOut()
          $.each result, (index, value) ->
            Smooster.page.get(value.id, Smooster.folder._id).done (result) ->
              data =
                id: value.id
                url: result.url
                date_value: result.date
                date: Smooster.page.find_name_in_content_elements(result.contentElements, date_id).body
                #author: Smooster.page.find_name_in_content_elements(result.contentElements, author_id).body
                headline: Smooster.page.find_name_in_content_elements(result.contentElements, headline_id).body
                picture: Smooster.page.find_name_in_content_elements(result.contentElements, picture_id).body
                teaser: Smooster.page.find_name_in_content_elements(result.contentElements, teaser_id).body
              new_pages.push(data)

          myTimer = setInterval(->
            if $(result).length == $(new_pages).length
              clearInterval(myTimer);

              new_pages.sort (a, b) ->
                keyA = new Date(a.date_value)
                keyB = new Date(b.date_value)

                # Compare the 2 dates
                return 1  if keyA < keyB
                return -1  if keyA > keyB
                0

              $.each new_pages, (index, value) ->
                  x = template.render(value)
                  $('#blog_posts').append(x)

              $('.next_links').remove()
              loadNextLink()
          , 10)

    $(more_button_id).click (event) =>
      event.preventDefault()
      load_next_content()
