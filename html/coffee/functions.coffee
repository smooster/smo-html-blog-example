# Just delete the lines below, which are not needed #
# BEGIN CONFIG #
# @codekit-prepend "lib/smo-mobile-nav.coffee"
# END CONFIG #

$(window).load ->
  $("body").removeClass("preload")

$(document).ready ->

  exampleFunction = ->
    console.log("smooster NOTICE: everything is running smooth.")

  selectBoxIt = ->
    unless typeof jQuery.selectBox == 'undefined'
      $("#selectboxit").selectBoxIt
                        autoWidth: false,
                        populate:
                            data: [
                              {
                                text: "Text1"
                                value: "Wert1"
                              }
                              {
                                text: "Text2"
                                value: "Wert2"
                              }
                              {
                                text: "Text2"
                                value: "Wert2"
                              }
                            ]
      $("#selectboxit").change (ev, obj)->
        console.log("handle the change of a selectbox")

  exampleFunction()

  #Optional scripts - delete what you don't need
  selectBoxIt()
